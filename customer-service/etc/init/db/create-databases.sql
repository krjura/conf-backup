DROP DATABASE IF EXISTS cb_customers;
CREATE DATABASE cb_customers ENCODING 'UTF-8';

DROP DATABASE IF EXISTS cb_customers_tst;
CREATE DATABASE cb_customers_tst ENCODING 'UTF-8';

DROP USER IF EXISTS cb_customers;
CREATE USER cb_customers WITH PASSWORD 'cb_customers';

DROP USER IF EXISTS cb_customers_tst;
CREATE USER cb_customers_tst WITH PASSWORD 'cb_customers_tst';