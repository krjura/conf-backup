CREATE TABLE organization (

  id BIGSERIAL not null,

  organization_id UUID NOT NULL,

  CONSTRAINT pk_organization PRIMARY KEY (id)
);

CREATE UNIQUE INDEX idx_organization_organization_id ON organization (organization_id);

CREATE TABLE organization_user (

  id BIGSERIAL not null,

  organization_id UUID NOT NULL,
  user_id UUID NOT NULL,
  user_secret VARCHAR(36) NOT NULL,

  CONSTRAINT pk_organization_user PRIMARY KEY (id)
);

CREATE UNIQUE INDEX idx_organization_user_user_id ON organization_user (user_id);

CREATE TABLE user_file (

  id BIGSERIAL not null,

  user_id UUID NOT NULL,
  file_group_id UUID NOT NULL,
  file_id UUID NOT NULL,
  relative_location VARCHAR(255) NOT NULL,
  file_sha_512_hash VARCHAR(128) NOT NULL,
  storage_type VARCHAR(16) NOT NULL,
  storage_reference VARCHAR(255) NOT NULL,

  CONSTRAINT pk_user_file PRIMARY KEY (id)
);

CREATE UNIQUE INDEX idx_user_file_file_id ON user_file (file_id);
CREATE INDEX idx_user_file_file_group_id ON user_file (file_group_id);