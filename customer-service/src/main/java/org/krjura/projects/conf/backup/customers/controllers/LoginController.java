package org.krjura.projects.conf.backup.customers.controllers;

import java.util.Objects;
import org.krjura.projects.conf.backup.customers.controllers.pojo.LoginRequest;
import org.krjura.projects.conf.backup.customers.controllers.pojo.LoginResponse;
import org.krjura.projects.conf.backup.customers.services.LoginService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Controller
public class LoginController {

    private final LoginService loginService;

    public LoginController(LoginService loginService) {
        this.loginService = Objects.requireNonNull(loginService);
    }

    @PostMapping(
            value = "/customer-service/api/login",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<LoginResponse> login(@RequestBody LoginRequest loginRequest) {

        var authData = this.loginService.login(
                loginRequest.getOrganizationId(),
                loginRequest.getUserId(),
                loginRequest.getUserSecret()
        );

        return ResponseEntity.ok(
                LoginResponse
                        .newBuilder()
                        .withAuthenticated(authData.isAuthenticated())
                        .withToken(authData.getToken())
                        .withTokenValidTo(authData.getTokenValidTo())
                        .build()
        );
    }
}
