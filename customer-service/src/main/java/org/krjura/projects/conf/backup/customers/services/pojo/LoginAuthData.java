package org.krjura.projects.conf.backup.customers.services.pojo;

import java.util.Objects;

public class LoginAuthData {

    private boolean authenticated;

    private String token;

    private Long tokenValidTo;

    private LoginAuthData(boolean authenticated, String token, Long tokenValidTo) {
        this.authenticated = authenticated;
        this.token = token;
        this.tokenValidTo = tokenValidTo;
    }

    public static LoginAuthData authenticated(String token, Long tokenValidTo) {
        Objects.requireNonNull(token);

        return new LoginAuthData(true, token, tokenValidTo);
    }

    public static LoginAuthData notAuthenticated() {
        return new LoginAuthData(false, null, null);
    }

    public boolean isAuthenticated() {
        return authenticated;
    }

    public String getToken() {
        return token;
    }

    public Long getTokenValidTo() {
        return tokenValidTo;
    }
}