package org.krjura.projects.conf.backup.customers.services;

import java.util.Objects;
import java.util.UUID;
import org.krjura.projects.conf.backup.customers.model.OrganizationUserModel;
import org.krjura.projects.conf.backup.customers.repository.OrganizationUserModelRepository;
import org.krjura.projects.conf.backup.customers.services.pojo.LoginAuthData;
import org.krjura.projects.conf.backup.customers.services.utils.LoginServiceLoginUtil;
import org.krjura.projects.conf.backup.jwt.JwtService;
import org.krjura.projects.conf.backup.jwt.pojos.SimpleTokenData;
import org.springframework.stereotype.Service;

@Service
public class LoginService {

    private final OrganizationUserModelRepository userRepository;

    private final JwtService jwtService;

    public LoginService(
            OrganizationUserModelRepository userRepository,
            JwtService jwtService) {

        this.userRepository = Objects.requireNonNull(userRepository);
        this.jwtService = Objects.requireNonNull(jwtService);
    }

    public LoginAuthData login(UUID organizationId, UUID userId, String userSecret) {

        var userOptional = userRepository.findByUserId(userId);

        if (userOptional.isEmpty()) {
            return LoginServiceLoginUtil.notAuthenticatedLoginAuthData();
        }

        var userFromDb = userOptional.get();

        if (!authValid(userFromDb, organizationId, userId, userSecret)) {
            return LoginServiceLoginUtil.notAuthenticatedLoginAuthData();
        }

        var tokenData = new SimpleTokenData(userFromDb.getOrganizationId(), userFromDb.getUserId());
        var tokenInfo = this.jwtService.createAuthToken(tokenData);

        return LoginAuthData.authenticated(tokenInfo.getToken(), tokenInfo.getExpiresAt());
    }

    private boolean authValid(
            OrganizationUserModel fromDb, UUID organizationId, UUID userId, String userSecret) {

        return fromDb.getOrganizationId().equals(organizationId)
                && fromDb.getUserId().equals(userId)
                && fromDb.getUserSecret().equals(userSecret);
    }
}