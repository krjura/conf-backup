package org.krjura.projects.conf.backup.customers.ex;

import java.util.UUID;

public class DataNotFoundException extends RuntimeException {

    public DataNotFoundException(String message) {
        super(message);
    }

    public DataNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public static DataNotFoundException of(UUID organizationId, UUID userId) {
        return new DataNotFoundException(
                "user not found for orgId: " + organizationId + " and userId: " + userId);
    }
}
