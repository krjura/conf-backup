package org.krjura.projects.conf.backup.customers.model.enums;

public enum StorageType {

    GCP, FILE_SYSTEM
}
