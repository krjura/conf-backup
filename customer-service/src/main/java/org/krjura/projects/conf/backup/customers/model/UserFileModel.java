package org.krjura.projects.conf.backup.customers.model;

import java.util.Objects;
import java.util.UUID;
import org.krjura.projects.conf.backup.customers.model.enums.StorageType;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

@Table("user_file")
public class UserFileModel {

    @Id
    private final Long id;

    @Column("user_id")
    private final UUID userId;

    @Column("file_group_id")
    private final UUID fileGroupId;

    @Column("file_id")
    private final UUID fileId;

    @Column("relative_location")
    private final String relativeLocation;

    @Column("file_sha_512_hash")
    private final String fileSha512Hash;

    @Column("storage_type")
    private final StorageType storageType;

    @Column("storage_reference")
    private final String storageReference;

    @PersistenceConstructor
    private UserFileModel(
            Long id,
            UUID userId,
            UUID fileGroupId,
            UUID fileId,
            String relativeLocation,
            String fileSha512Hash,
            StorageType storageType,
            String storageReference) {

        this.id = id;
        this.userId = Objects.requireNonNull(userId);
        this.fileGroupId = Objects.requireNonNull(fileGroupId);
        this.fileId = Objects.requireNonNull(fileId);
        this.relativeLocation = Objects.requireNonNull(relativeLocation);
        this.fileSha512Hash = Objects.requireNonNull(fileSha512Hash);
        this.storageType = Objects.requireNonNull(storageType);
        this.storageReference = Objects.requireNonNull(storageReference);
    }

    private UserFileModel(Builder builder) {
        this(
                builder.id,
                builder.userId,
                builder.fileGroupId,
                builder.fileId,
                builder.relativeLocation,
                builder.fileSha512Hash,
                builder.storageType,
                builder.storageReference
        );
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public UserFileModel withId(Long id) {
        return new UserFileModel(
                id,
                this.userId,
                this.fileGroupId,
                this.fileId,
                this.relativeLocation,
                this.fileSha512Hash,
                this.storageType,
                this.storageReference
        );
    }

    public Long getId() {
        return id;
    }

    public UUID getUserId() {
        return userId;
    }

    public UUID getFileGroupId() {
        return fileGroupId;
    }

    public UUID getFileId() {
        return fileId;
    }

    public String getRelativeLocation() {
        return relativeLocation;
    }

    public String getFileSha512Hash() {
        return fileSha512Hash;
    }

    public StorageType getStorageType() {
        return storageType;
    }

    public String getStorageReference() {
        return storageReference;
    }


    public static final class Builder {
        private Long id;
        private UUID userId;
        private UUID fileGroupId;
        private UUID fileId;
        private String relativeLocation;
        private String fileSha512Hash;
        private StorageType storageType;
        private String storageReference;

        private Builder() {
        }

        public Builder withId(Long val) {
            id = val;
            return this;
        }

        public Builder withUserId(UUID val) {
            userId = val;
            return this;
        }

        public Builder withFileGroupId(UUID val) {
            fileGroupId = val;
            return this;
        }

        public Builder withFileId(UUID val) {
            fileId = val;
            return this;
        }

        public Builder withRelativeLocation(String val) {
            relativeLocation = val;
            return this;
        }

        public Builder withFileSha512Hash(String val) {
            fileSha512Hash = val;
            return this;
        }

        public Builder withStorageType(StorageType val) {
            storageType = val;
            return this;
        }

        public Builder withStorageReference(String val) {
            storageReference = val;
            return this;
        }

        public UserFileModel build() {
            return new UserFileModel(this);
        }
    }
}