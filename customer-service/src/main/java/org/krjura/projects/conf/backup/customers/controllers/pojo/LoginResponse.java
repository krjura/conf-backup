package org.krjura.projects.conf.backup.customers.controllers.pojo;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginResponse {

    private final boolean authenticated;

    private final String token;

    private final Long tokenValidTo;

    @JsonCreator
    private LoginResponse(
            @JsonProperty("authenticated") boolean authenticated,
            @JsonProperty("token") String token,
            @JsonProperty("tokenValidTo") Long tokenValidTo) {

        this.authenticated = authenticated;
        this.token = token;
        this.tokenValidTo = tokenValidTo;
    }

    private LoginResponse(Builder builder) {
        this(builder.authenticated, builder.token, builder.tokenValidTo);
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public boolean isAuthenticated() {
        return authenticated;
    }

    public String getToken() {
        return token;
    }

    public Long getTokenValidTo() {
        return tokenValidTo;
    }

    public static final class Builder {
        private boolean authenticated;
        private String token;
        private Long tokenValidTo;

        private Builder() {
        }

        public Builder withAuthenticated(boolean val) {
            authenticated = val;
            return this;
        }

        public Builder withToken(String val) {
            token = val;
            return this;
        }

        public Builder withTokenValidTo(Long val) {
            tokenValidTo = val;
            return this;
        }

        public LoginResponse build() {
            return new LoginResponse(this);
        }
    }
}