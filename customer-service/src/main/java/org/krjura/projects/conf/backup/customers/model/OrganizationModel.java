package org.krjura.projects.conf.backup.customers.model;

import java.util.Objects;
import java.util.UUID;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

@Table("organization")
public class OrganizationModel {

    @Id
    private final Long id;

    @Column("organization_id")
    private final UUID organizationId;

    @PersistenceConstructor
    private OrganizationModel(Long id, UUID organizationId) {
        this.id = id;
        this.organizationId = Objects.requireNonNull(organizationId);
    }

    private OrganizationModel(Builder builder) {
        this(
                builder.id,
                builder.organizationId
        );
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    // used by spring data to set id after save
    public OrganizationModel withId(Long id) {
        return new OrganizationModel(
                id,
                this.organizationId
        );
    }

    public UUID getOrganizationId() {
        return organizationId;
    }

    public Long getId() {
        return id;
    }

    public static final class Builder {
        private Long id;
        private UUID organizationId;

        private Builder() {
        }

        public Builder withId(Long val) {
            id = val;
            return this;
        }

        public Builder withOrganizationId(UUID val) {
            organizationId = val;
            return this;
        }

        public OrganizationModel build() {
            return new OrganizationModel(this);
        }
    }
}