package org.krjura.projects.conf.backup.customers.services.utils;

import org.krjura.projects.conf.backup.customers.services.pojo.LoginAuthData;

public class LoginServiceLoginUtil {

    private LoginServiceLoginUtil() {
        // util
    }

    public static LoginAuthData notAuthenticatedLoginAuthData() {
        return LoginAuthData.notAuthenticated();
    }
}
