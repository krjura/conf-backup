package org.krjura.projects.conf.backup.customers.controllers.pojo;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;
import java.util.UUID;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginRequest {

    private final UUID organizationId;
    private final UUID userId;
    private final String userSecret;

    @JsonCreator
    public LoginRequest(
            @JsonProperty("organizationId") UUID organizationId,
            @JsonProperty("userId") UUID userId,
            @JsonProperty("userSecret") String userSecret) {

        this.organizationId = Objects.requireNonNull(organizationId);
        this.userId = Objects.requireNonNull(userId);
        this.userSecret = Objects.requireNonNull(userSecret);
    }

    public UUID getOrganizationId() {
        return organizationId;
    }

    public UUID getUserId() {
        return userId;
    }

    public String getUserSecret() {
        return userSecret;
    }
}