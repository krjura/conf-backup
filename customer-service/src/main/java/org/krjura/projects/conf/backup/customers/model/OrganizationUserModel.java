package org.krjura.projects.conf.backup.customers.model;

import java.util.Objects;
import java.util.UUID;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

@Table("organization_user")
public class OrganizationUserModel {

    @Id
    private final Long id;

    @Column("organization_id")
    private final UUID organizationId;

    @Column("user_id")
    private final UUID userId;

    @Column("user_secret")
    private final String userSecret;

    @PersistenceConstructor
    private OrganizationUserModel(Long id, UUID organizationId, UUID userId, String userSecret) {
        this.id = id;
        this.organizationId = Objects.requireNonNull(organizationId);
        this.userId = Objects.requireNonNull(userId);
        this.userSecret = Objects.requireNonNull(userSecret);
    }

    private OrganizationUserModel(Builder builder) {
        this(
                builder.id,
                builder.organizationId,
                builder.userId,
                builder.userSecret
        );
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    // for data jdbc to avoid reflection
    public OrganizationUserModel withId(Long id) {
        return new OrganizationUserModel(
                id,
                this.organizationId,
                this.userId,
                this.userSecret
        );
    }

    public Long getId() {
        return id;
    }

    public UUID getOrganizationId() {
        return organizationId;
    }

    public UUID getUserId() {
        return userId;
    }

    public String getUserSecret() {
        return userSecret;
    }


    public static final class Builder {
        private Long id;
        private UUID organizationId;
        private UUID userId;
        private String userSecret;

        private Builder() {
        }

        public Builder withId(Long val) {
            id = val;
            return this;
        }

        public Builder withOrganizationId(UUID val) {
            organizationId = val;
            return this;
        }

        public Builder withUserId(UUID val) {
            userId = val;
            return this;
        }

        public Builder withUserSecret(String val) {
            userSecret = val;
            return this;
        }

        public OrganizationUserModel build() {
            return new OrganizationUserModel(this);
        }
    }
}
