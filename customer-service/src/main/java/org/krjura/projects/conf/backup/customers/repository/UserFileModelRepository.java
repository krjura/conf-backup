package org.krjura.projects.conf.backup.customers.repository;

import org.krjura.projects.conf.backup.customers.model.UserFileModel;
import org.springframework.data.repository.CrudRepository;

public interface UserFileModelRepository extends CrudRepository<UserFileModel, Long> {


}
