package org.krjura.projects.conf.backup.customers.repository;

import java.util.Optional;
import java.util.UUID;
import org.krjura.projects.conf.backup.customers.model.OrganizationUserModel;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface OrganizationUserModelRepository
        extends CrudRepository<OrganizationUserModel, Long> {

    @Query("SELECT e.* FROM organization_user e WHERE e.user_id = :userId")
    Optional<OrganizationUserModel> findByUserId(@Param("userId") UUID userId);
}
