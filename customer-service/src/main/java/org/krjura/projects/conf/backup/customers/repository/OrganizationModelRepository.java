package org.krjura.projects.conf.backup.customers.repository;

import java.util.Optional;
import java.util.UUID;
import org.krjura.projects.conf.backup.customers.model.OrganizationModel;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface OrganizationModelRepository extends CrudRepository<OrganizationModel, Long> {

    @Query("SELECT e.* from organization e WHERE e.organization_id = :organizationId")
    Optional<OrganizationModel> findByOrgId(@Param("organizationId") UUID orgId);
}
