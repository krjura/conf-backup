package org.krjura.projects.conf.backup.customers.controllers;

import static com.atlassian.oai.validator.mockmvc.OpenApiValidationMatchers.openApi;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.krjura.projects.conf.backup.customers.controllers.pojo.LoginRequest;
import org.krjura.projects.conf.backup.customers.controllers.pojo.LoginResponse;
import org.krjura.projects.conf.backup.customers.model.OrganizationUserModel;
import org.krjura.projects.conf.backup.customers.repository.OrganizationUserModelRepository;
import org.krjura.projects.conf.backup.customers.support.OpenApiUtil;
import org.krjura.projects.conf.backup.customers.support.WebTestBase;
import org.krjura.projects.conf.backup.jwt.JwtService;
import org.krjura.projects.conf.backup.jwt.config.JwtConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;

public class LoginControllerTest extends WebTestBase {

    @Autowired
    private OrganizationUserModelRepository userRepository;

    @Autowired
    private JwtService jwtService;

    @Autowired
    private JwtConfig jwtConfig;

    @Test
    public void expectUserIsAuthenticatedWhenFoundAndPasswordInvalid() throws Exception {
        // add data
        var user = addDefaultUserToDb();

        // send request
        var request = new LoginRequest(user.getOrganizationId(), user.getUserId(), "invalid");

        MvcResult result = getMvc().perform(
                post("/customer-service/api/login")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(toJson(request)))
                .andExpect(status().is(200))
                .andExpect(openApi().isValid(OpenApiUtil.validator))
                .andReturn();

        // check data
        LoginResponse response = fromJson(result, LoginResponse.class);
        assertThat(response).isNotNull();
        assertThat(response.isAuthenticated()).isFalse();
        assertThat(response.getToken()).isNull();
        assertThat(response.getTokenValidTo()).isNull();
    }

    @Test
    public void expectUserIsAuthenticatedWhenFound() throws Exception {
        // add data
        var user = addDefaultUserToDb();

        // send request
        var request = new LoginRequest(
                user.getOrganizationId(), user.getUserId(), user.getUserSecret()
        );

        MvcResult result = getMvc().perform(
                post("/customer-service/api/login")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(toJson(request)))
                .andExpect(status().is(200))
                .andExpect(openApi().isValid(OpenApiUtil.validator))
                .andReturn();

        // check data
        LoginResponse response = fromJson(result, LoginResponse.class);
        assertThat(response).isNotNull();
        assertThat(response.isAuthenticated()).isTrue();
        assertThat(response.getToken()).isNotNull();
        assertThat(response.getTokenValidTo()).isGreaterThan(0L);

        var tokenInfo = jwtService.parseToken(response.getToken());
        assertThat(tokenInfo).isNotNull();
        assertThat(tokenInfo.getUserId()).isEqualTo(user.getUserId());
        assertThat(tokenInfo.getOrganizationId()).isEqualTo(user.getOrganizationId());
        assertThat(tokenInfo.getKeyId()).isEqualTo(jwtConfig.getDefaultKeyId());
        assertThat(tokenInfo.getExpiresAt()).isNotNull();
        assertThat(tokenInfo.getIssuedAt()).isNotNull();
        assertThat(tokenInfo.getNotBefore()).isNotNull();
    }

    @Test
    public void expectUserIsNotAuthenticatedWhenNotFound() throws Exception {
        var request = new LoginRequest(UUID.randomUUID(), UUID.randomUUID(), "secret");

        MvcResult result = getMvc().perform(
                post("/customer-service/api/login")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(toJson(request)))
                .andExpect(status().is(200))
                .andExpect(openApi().isValid(OpenApiUtil.validator))
                .andReturn();

        LoginResponse response = fromJson(result, LoginResponse.class);
        assertThat(response).isNotNull();
        assertThat(response.isAuthenticated()).isFalse();
        assertThat(response.getToken()).isNull();
        assertThat(response.getTokenValidTo()).isNull();
    }

    private OrganizationUserModel addDefaultUserToDb() {
        var user = OrganizationUserModel
                .newBuilder()
                .withOrganizationId(UUID.randomUUID())
                .withUserId(UUID.randomUUID())
                .withUserSecret("secret")
                .build();

        return this.userRepository.save(user);
    }
}