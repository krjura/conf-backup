package org.krjura.projects.conf.backup.customers.controllers;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import org.junit.jupiter.api.Test;
import org.krjura.projects.conf.backup.customers.support.WebTestBase;
import org.springframework.test.web.servlet.MvcResult;

public class OpenApiControllerTest extends WebTestBase {

    @Test
    public void checkIfOpenApiIsServed() throws Exception {

        MvcResult result = getMvc().perform(
                get("/customer-service/openapi"))
                .andExpect(status().is(200))
                .andReturn();

        byte[] data = result.getResponse().getContentAsByteArray();
        byte[] expectedData = contentOf("openapi.yaml");

        System.out.println(new String(data));

        assertThat(Arrays.equals(data, expectedData)).isTrue();
    }
}
