package org.krjura.projects.conf.backup.customers.support;

import com.atlassian.oai.validator.OpenApiInteractionValidator;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Objects;

public class OpenApiUtil {

    public static final OpenApiInteractionValidator validator;

    static {
        try {
            URL url = TestBase.class.getClassLoader().getResource("openapi.yaml");
            Objects.requireNonNull(url);

            byte[] openApiData = Files.readAllBytes(Paths.get(url.toURI()));

            validator = OpenApiInteractionValidator
                    .createForInlineApiSpecification(utf8String(openApiData))
                    .build();
        } catch (URISyntaxException e) {
            throw new IllegalArgumentException("invalid openapi path classpath:openapi.yaml");
        } catch (IOException e) {
            throw new IllegalArgumentException(
                    "cannot read openapi file from path classpath:openapi.yaml"
            );
        }
    }

    private static String utf8String(byte[] openApiData) {
        return new String(openApiData, StandardCharsets.UTF_8);
    }
}
