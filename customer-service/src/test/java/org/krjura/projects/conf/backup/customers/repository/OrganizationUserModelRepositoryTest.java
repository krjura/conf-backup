package org.krjura.projects.conf.backup.customers.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.krjura.projects.conf.backup.customers.model.OrganizationUserModel;
import org.krjura.projects.conf.backup.customers.support.TestBase;
import org.springframework.beans.factory.annotation.Autowired;

public class OrganizationUserModelRepositoryTest extends TestBase {

    @Autowired
    private OrganizationUserModelRepository repository;

    @Test
    public void testRepositoryMethods() {
        var orgId = UUID.randomUUID();
        var userId = UUID.randomUUID();

        var model = OrganizationUserModel
                .newBuilder()
                .withOrganizationId(orgId)
                .withUserId(userId)
                .withUserSecret("secret")
                .build();

        var savedModel = this.repository.save(model);

        checkFindById(savedModel);
        checkFindByUserId(savedModel);
    }

    private void checkFindByUserId(OrganizationUserModel savedModel) {
        var located = this.repository.findByUserId(savedModel.getUserId());
        assertThat(located).isPresent();

        verifyModel(savedModel, located.get());
    }

    private void checkFindById(OrganizationUserModel savedModel) {
        var located = this.repository.findById(savedModel.getId());
        assertThat(located).isPresent();

        verifyModel(savedModel, located.get());
    }

    private void verifyModel(OrganizationUserModel savedModel,OrganizationUserModel located) {

        assertThat(located.getId()).isEqualTo(savedModel.getId());
        assertThat(located.getOrganizationId()).isEqualTo(savedModel.getOrganizationId());
        assertThat(located.getUserId()).isEqualTo(savedModel.getUserId());
        assertThat(located.getUserSecret()).isEqualTo(savedModel.getUserSecret());
    }
}