package org.krjura.projects.conf.backup.customers.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.krjura.projects.conf.backup.customers.model.UserFileModel;
import org.krjura.projects.conf.backup.customers.model.enums.StorageType;
import org.krjura.projects.conf.backup.customers.support.TestBase;
import org.springframework.beans.factory.annotation.Autowired;

public class UserFileModelRepositoryTest extends TestBase {

    @Autowired
    private UserFileModelRepository repository;

    @Test
    public void testRepositoryMethods() {
        var model = UserFileModel
                .newBuilder()
                .withUserId(UUID.randomUUID())
                .withFileGroupId(UUID.randomUUID())
                .withFileId(UUID.randomUUID())
                .withRelativeLocation("/tmp/demo.txt")
                .withFileSha512Hash("aaaaaaaaa")
                .withStorageType(StorageType.GCP)
                .withStorageReference("gcp://repo/file")
                .build();

        var saved = this.repository.save(model);

        checkFindById(saved);

    }

    private void checkFindById(UserFileModel saved) {
        var located = this.repository.findById(saved.getId());
        assertThat(located).isPresent();

        verifyModel(saved, located.get());
    }

    private void verifyModel(UserFileModel saved, UserFileModel located) {
        assertThat(saved.getId()).isEqualTo(located.getId());
        assertThat(saved.getFileGroupId()).isEqualTo(located.getFileGroupId());
        assertThat(saved.getFileId()).isEqualTo(located.getFileId());
        assertThat(saved.getRelativeLocation()).isEqualTo(located.getRelativeLocation());
        assertThat(saved.getFileSha512Hash()).isEqualTo(located.getFileSha512Hash());
        assertThat(saved.getStorageType()).isEqualTo(located.getStorageType());
        assertThat(saved.getStorageReference()).isEqualTo(located.getStorageReference());
    }
}