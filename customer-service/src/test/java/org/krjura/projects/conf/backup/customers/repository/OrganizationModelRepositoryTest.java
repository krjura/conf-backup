package org.krjura.projects.conf.backup.customers.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.krjura.projects.conf.backup.customers.model.OrganizationModel;
import org.krjura.projects.conf.backup.customers.support.TestBase;
import org.springframework.beans.factory.annotation.Autowired;

public class OrganizationModelRepositoryTest extends TestBase {

    @Autowired
    private OrganizationModelRepository repository;

    @Test
    public void testRepositoryMethods() {
        var orgId = UUID.randomUUID();

        var model = OrganizationModel.newBuilder().withOrganizationId(orgId).build();

        var savedModel = repository.save(model);
        assertThat(savedModel).isNotNull();
        assertThat(savedModel.getId()).isNotNull();

        findByOrgIdCheck(orgId);
        findByIdCheck(savedModel);
    }

    private void findByIdCheck(OrganizationModel savedModel) {
        var model = this.repository.findById(savedModel.getId());

        assertThat(model).isPresent();
        assertThat(model.get().getId()).isEqualTo(savedModel.getId());
        assertThat(model.get().getOrganizationId()).isEqualTo(savedModel.getOrganizationId());
    }

    private void findByOrgIdCheck(UUID orgId) {
        var locatedModel = this.repository.findByOrgId(orgId);

        assertThat(locatedModel).isPresent();
        assertThat(locatedModel.get().getOrganizationId()).isEqualTo(orgId);
    }

}