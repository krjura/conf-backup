package org.krjura.projects.conf.backup.customers.support;

import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

public abstract class WebTestBase extends TestBase {

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mvc;

    @BeforeEach
    public void beforeWebTestBase() {
        this.mvc = MockMvcBuilders
                .webAppContextSetup(this.wac)
                .build();
    }

    public MockMvc getMvc() {
        return mvc;
    }
}
