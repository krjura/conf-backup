package org.krjura.projects.conf.backup.customers.support;

public class TestingException extends RuntimeException {

    public TestingException(String message) {
        super(message);
    }

    public TestingException(String message, Throwable cause) {
        super(message, cause);
    }
}
