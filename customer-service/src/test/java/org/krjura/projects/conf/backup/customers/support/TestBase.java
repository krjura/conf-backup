package org.krjura.projects.conf.backup.customers.support;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Paths;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MvcResult;

@ExtendWith(SpringExtension.class)
@ProjectTest
public abstract class TestBase {

    @Autowired
    private ObjectMapper objectMapper;

    protected <T> T fromJson(MvcResult result, Class<T> clazz) {
        try {
            return fromJson(result.getResponse().getContentAsString(), clazz);
        } catch (UnsupportedEncodingException e) {
            throw new TestingException("cannot convert json to object", e);
        }
    }

    protected <T> T fromJson(String content, Class<T> clazz) {
        try {
            return this.objectMapper.readValue(content, clazz);
        } catch (IOException e) {
            throw new TestingException("cannot convert json to object", e);
        }
    }

    protected String toJson(Object data) {
        try {
            return this.objectMapper.writeValueAsString(data);
        } catch (JsonProcessingException e) {
            throw new TestingException("cannot convert object to json", e);
        }
    }

    public byte[] contentOf(String fileName) {
        try {
            return Files.readAllBytes(
                    Paths.get(TestBase.class.getClassLoader().getResource(fileName).toURI())
            );
        } catch (Exception e) {
            throw new TestingException(
                    "unable to read content of file " + fileName + " from classpath", e);
        }
    }
}
