package org.krjura.projects.conf.backup.jwt.pojos;

import java.util.Date;
import java.util.Objects;
import java.util.UUID;

public class TokenData {

    private String keyId;

    private final UUID organizationId;

    private final UUID userId;

    private final String issuer;

    private final Date expiresAt;

    private final Date issuedAt;

    private final Date notBefore;

    public TokenData(
            String keyId,
            UUID organizationId,
            UUID userId,
            String issuer,
            Date expiresAt,
            Date issuedAt,
            Date notBefore) {

        this.keyId = Objects.requireNonNull(keyId);
        this.organizationId = Objects.requireNonNull(organizationId);
        this.userId = Objects.requireNonNull(userId);
        this.issuer = Objects.requireNonNull(issuer);
        this.expiresAt = Objects.requireNonNull(expiresAt);
        this.issuedAt = Objects.requireNonNull(issuedAt);
        this.notBefore = Objects.requireNonNull(notBefore);
    }

    private TokenData(Builder builder) {
        this(
                builder.keyId,
                builder.organizationId,
                builder.userId,
                builder.issuer,
                builder.expiresAt,
                builder.issuedAt,
                builder.notBefore
        );
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public String getKeyId() {
        return keyId;
    }

    public UUID getOrganizationId() {
        return organizationId;
    }

    public UUID getUserId() {
        return userId;
    }

    public String getIssuer() {
        return issuer;
    }

    public Date getExpiresAt() {
        return expiresAt;
    }

    public Date getIssuedAt() {
        return issuedAt;
    }

    public Date getNotBefore() {
        return notBefore;
    }

    public static final class Builder {
        private String keyId;
        private UUID organizationId;
        private UUID userId;
        private String issuer;
        private Date expiresAt;
        private Date issuedAt;
        private Date notBefore;

        private Builder() {
        }

        public Builder withKeyId(String val) {
            keyId = val;
            return this;
        }

        public Builder withOrganizationId(UUID val) {
            organizationId = val;
            return this;
        }

        public Builder withUserId(UUID val) {
            userId = val;
            return this;
        }

        public Builder withIssuer(String val) {
            issuer = val;
            return this;
        }

        public Builder withExpiresAt(Date val) {
            expiresAt = val;
            return this;
        }

        public Builder withIssuedAt(Date val) {
            issuedAt = val;
            return this;
        }

        public Builder withNotBefore(Date val) {
            notBefore = val;
            return this;
        }

        public TokenData build() {
            return new TokenData(this);
        }
    }
}
