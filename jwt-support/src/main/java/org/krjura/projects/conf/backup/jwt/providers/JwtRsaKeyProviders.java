package org.krjura.projects.conf.backup.jwt.providers;

import static java.util.stream.Collectors.toMap;

import java.util.Map;
import java.util.Optional;
import org.krjura.projects.conf.backup.jwt.config.JwtConfig;

public class JwtRsaKeyProviders {

    private Map<String, JwtRsaKeyProvider> providers;

    public JwtRsaKeyProviders(JwtConfig config) {
        this.providers = config
                .getKeys()
                .entrySet()
                .stream()
                .collect(toMap(Map.Entry::getKey, JwtRsaKeyProviderFactory::fromEntry));
    }

    public Map<String, JwtRsaKeyProvider> getProviders() {
        return providers;
    }

    public Optional<JwtRsaKeyProvider> getProvider(String keyId) {
        return Optional.ofNullable(this.providers.get(keyId));
    }
}
