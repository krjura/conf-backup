package org.krjura.projects.conf.backup.jwt.pojos;

import java.util.Objects;

public class TokenInfo {

    private final String token;

    private final long expiresAt;

    public TokenInfo(String token, long expiresAt) {
        this.token = Objects.requireNonNull(token);
        this.expiresAt = expiresAt;
    }

    public long getExpiresAt() {
        return expiresAt;
    }

    public String getToken() {
        return token;
    }
}
