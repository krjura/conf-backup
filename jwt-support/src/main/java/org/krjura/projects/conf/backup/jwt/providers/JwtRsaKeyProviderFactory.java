package org.krjura.projects.conf.backup.jwt.providers;

import java.util.Map;
import org.krjura.projects.conf.backup.jwt.config.JwtConfigKey;

class JwtRsaKeyProviderFactory {

    private JwtRsaKeyProviderFactory() {
        // factory
    }

    static JwtRsaKeyProvider fromEntry(Map.Entry<String, JwtConfigKey> entry) {
        return new JwtRsaKeyProvider(entry.getKey(), entry.getValue());
    }

}