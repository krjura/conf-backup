package org.krjura.projects.conf.backup.jwt;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;
import org.krjura.projects.conf.backup.jwt.config.JwtConfig;
import org.krjura.projects.conf.backup.jwt.ex.JwtException;
import org.krjura.projects.conf.backup.jwt.pojos.SimpleTokenData;
import org.krjura.projects.conf.backup.jwt.pojos.TokenData;
import org.krjura.projects.conf.backup.jwt.pojos.TokenInfo;
import org.krjura.projects.conf.backup.jwt.providers.JwtRsaKeyProviders;

public class JwtService {

    private final JwtConfig jwtConfig;

    private final JwtRsaKeyProviders jwtRsaKeyProviders;

    public JwtService(JwtConfig jwtConfig, JwtRsaKeyProviders jwtRsaKeyProviders) {
        this.jwtConfig = Objects.requireNonNull(jwtConfig);
        this.jwtRsaKeyProviders = Objects.requireNonNull(jwtRsaKeyProviders);
    }

    public TokenInfo createAuthToken(SimpleTokenData info) {
        String defaultKeyId = this.jwtConfig.getDefaultKeyId();
        String defaultIssuer = this.jwtConfig.getDefaultIssuer();

        var provider = this
                .jwtRsaKeyProviders.getProvider(defaultKeyId)
                .orElseThrow(() ->
                        new JwtException("unable to find key with id of " + defaultKeyId));

        var algorithm = Algorithm.RSA256(provider);

        ZonedDateTime now = ZonedDateTime.now();
        Date current = Date.from(now.toInstant());
        Date expiresAt = Date.from(
                now.plusSeconds(this.jwtConfig.getDefaultDurationInSeconds()).toInstant()
        );

        String token = JWT.create()
                .withClaim(JwtConst.CONST_ORGANIZATION_ID, info.getOrganizationId().toString())
                .withClaim(JwtConst.CONST_USER_ID, info.getUserId().toString())
                .withKeyId(defaultKeyId)
                .withAudience(JwtConst.CONST_AUDIENCE_ANY)
                .withExpiresAt(expiresAt)
                .withIssuedAt(current)
                .withNotBefore(current)
                .withIssuer(defaultIssuer)
                .sign(algorithm);

        return new TokenInfo(token, expiresAt.getTime());
    }

    public TokenData parseToken(String token) {
        var keyId = Objects.requireNonNull(parseKey(token));
        var jwtVerifier = buildJwtVerifier(keyId);
        var decodedJwt = jwtVerifier.verify(token);

        // validation of data is done in TokenData
        return TokenData
                .newBuilder()
                .withKeyId(keyId)
                .withOrganizationId(uuidFromClaim(decodedJwt, JwtConst.CONST_ORGANIZATION_ID))
                .withUserId(uuidFromClaim(decodedJwt, JwtConst.CONST_USER_ID))
                .withIssuer(decodedJwt.getIssuer())
                .withExpiresAt(decodedJwt.getExpiresAt())
                .withIssuedAt(decodedJwt.getIssuedAt())
                .withNotBefore(decodedJwt.getNotBefore())
                .build();
    }

    private UUID uuidFromClaim(DecodedJWT decodedJwt, String claim) {
        return UUID.fromString(decodedJwt.getClaim(claim).asString());
    }

    private JWTVerifier buildJwtVerifier(String keyId) {
        var provider = this
                .jwtRsaKeyProviders
                .getProvider(keyId)
                .orElseThrow(() -> new JwtException("key with id " + keyId + " not found"));

        Algorithm algorithm = Algorithm.RSA256(provider);

        return JWT
                .require(algorithm)
                .acceptLeeway(10)
                .acceptExpiresAt(10)
                .acceptIssuedAt(10)
                .acceptNotBefore(10)
                .build();
    }

    private String parseKey(String token) {
        return JWT.decode(token).getKeyId();
    }
}
