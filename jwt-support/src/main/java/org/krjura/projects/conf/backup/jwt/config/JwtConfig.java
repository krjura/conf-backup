package org.krjura.projects.conf.backup.jwt.config;

import java.util.Map;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "org.krjura.jwt")
@ConditionalOnProperty(value = "org.krjura.jwt.enabled", havingValue = "true")
public class JwtConfig {

    private boolean enabled;

    private String defaultKeyId;

    private String defaultIssuer;

    // 1h;
    private long defaultDurationInSeconds = 3600;

    private Map<String, JwtConfigKey> keys;

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getDefaultKeyId() {
        return defaultKeyId;
    }

    public void setDefaultKeyId(String defaultKeyId) {
        this.defaultKeyId = defaultKeyId;
    }

    public String getDefaultIssuer() {
        return defaultIssuer;
    }

    public void setDefaultIssuer(String defaultIssuer) {
        this.defaultIssuer = defaultIssuer;
    }

    public long getDefaultDurationInSeconds() {
        return defaultDurationInSeconds;
    }

    public void setDefaultDurationInSeconds(long defaultDurationInSeconds) {
        this.defaultDurationInSeconds = defaultDurationInSeconds;
    }

    public Map<String, JwtConfigKey> getKeys() {
        return keys;
    }

    public void setKeys(Map<String, JwtConfigKey> keys) {
        this.keys = keys;
    }
}
