package org.krjura.projects.conf.backup.jwt.pojos;

import java.util.Objects;
import java.util.UUID;

public class SimpleTokenData {

    private final UUID organizationId;

    private final UUID userId;

    public SimpleTokenData(UUID organizationId, UUID userId) {

        this.organizationId = Objects.requireNonNull(organizationId);
        this.userId = Objects.requireNonNull(userId);
    }

    public UUID getOrganizationId() {
        return organizationId;
    }

    public UUID getUserId() {
        return userId;
    }
}
