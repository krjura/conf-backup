package org.krjura.projects.conf.backup.jwt;

import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.Objects;

public class KeyStorage {

    private final RSAPublicKey publicKey;
    private final String encodedPublicKey;

    private final RSAPrivateKey privateKey;
    private final String encodedPrivateKey;

    public KeyStorage(
            RSAPublicKey publicKey,
            String encodedPublicKey,
            RSAPrivateKey privateKey,
            String encodedPrivateKey) {

        this.publicKey = Objects.requireNonNull(publicKey);
        this.encodedPublicKey = Objects.requireNonNull(encodedPublicKey);
        this.privateKey = Objects.requireNonNull(privateKey);
        this.encodedPrivateKey = Objects.requireNonNull(encodedPrivateKey);
    }

    public RSAPublicKey getPublicKey() {
        return publicKey;
    }

    public String getEncodedPublicKey() {
        return encodedPublicKey;
    }

    public RSAPrivateKey getPrivateKey() {
        return privateKey;
    }

    public String getEncodedPrivateKey() {
        return encodedPrivateKey;
    }
}
