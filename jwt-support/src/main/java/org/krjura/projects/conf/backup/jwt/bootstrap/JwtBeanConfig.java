package org.krjura.projects.conf.backup.jwt.bootstrap;

import org.krjura.projects.conf.backup.jwt.JwtService;
import org.krjura.projects.conf.backup.jwt.config.JwtConfig;
import org.krjura.projects.conf.backup.jwt.providers.JwtRsaKeyProviders;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnProperty(value = "org.krjura.jwt.enabled", havingValue = "true")
public class JwtBeanConfig {

    @Bean
    public JwtRsaKeyProviders jwtRsaKeyProviders(JwtConfig config) {
        return new JwtRsaKeyProviders(config);
    }

    @Bean
    public JwtService jwtService(JwtConfig config, JwtRsaKeyProviders providers) {
        return new JwtService(config, providers);
    }
}
