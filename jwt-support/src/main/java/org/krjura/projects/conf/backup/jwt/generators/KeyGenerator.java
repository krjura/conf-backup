package org.krjura.projects.conf.backup.jwt.generators;

import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import org.krjura.projects.conf.backup.jwt.KeyStorage;

public class KeyGenerator {

    public static KeyStorage generate() throws NoSuchAlgorithmException, InvalidKeySpecException {
        KeyPairGenerator rsaGenerator = KeyPairGenerator.getInstance("RSA");
        rsaGenerator.initialize(2048);
        KeyPair rsaKeyPair = rsaGenerator.generateKeyPair();

        byte[] publicKey = rsaKeyPair.getPublic().getEncoded();
        byte[] privateKey = rsaKeyPair.getPrivate().getEncoded();
        String encodedPublicKey = Base64.getEncoder().encodeToString(publicKey);
        String encodedPrivateKey = Base64.getEncoder().encodeToString(privateKey);

        X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(publicKey);
        PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(privateKey);

        KeyFactory kf = KeyFactory.getInstance("RSA");
        RSAPrivateKey rsaPrivateKey = (RSAPrivateKey) kf.generatePrivate(privateKeySpec);
        RSAPublicKey rsaPublicKey = (RSAPublicKey) kf.generatePublic(publicKeySpec);

        return new KeyStorage(rsaPublicKey, encodedPublicKey, rsaPrivateKey, encodedPrivateKey);
    }

    public static void main(String[] args)
            throws InvalidKeySpecException, NoSuchAlgorithmException {

        var key = KeyGenerator.generate();
        System.out.println("private key: " + key.getEncodedPrivateKey());
        System.out.println("public key: " + key.getEncodedPublicKey());
    }
}
