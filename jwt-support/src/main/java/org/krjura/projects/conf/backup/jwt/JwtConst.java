package org.krjura.projects.conf.backup.jwt;

public class JwtConst {

    public static final String CONST_ORGANIZATION_ID = "organizationId";
    public static final String CONST_USER_ID = "userId";
    public static final String CONST_AUDIENCE_ANY = "any";

    private JwtConst() {
        // const
    }
}
