package org.krjura.projects.conf.backup.jwt.providers;

import com.auth0.jwt.interfaces.RSAKeyProvider;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import org.krjura.projects.conf.backup.jwt.config.JwtConfigKey;
import org.krjura.projects.conf.backup.jwt.ex.JwtException;

public class JwtRsaKeyProvider implements RSAKeyProvider {

    private final String keyId;
    private final RSAPublicKey publicKey;
    private final RSAPrivateKey privateKey;

    public JwtRsaKeyProvider(String keyId, JwtConfigKey key) {
        this.keyId = keyId;

        try {
            byte[] publicKeyBytes = Base64.getDecoder().decode(key.getPublicKey());
            X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(publicKeyBytes);

            byte[] privateKeyBytes = Base64.getDecoder().decode(key.getPrivateKey());
            PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(privateKeyBytes);

            KeyFactory kf = KeyFactory.getInstance("RSA");
            this.publicKey = (RSAPublicKey) kf.generatePublic(publicKeySpec);
            this.privateKey = (RSAPrivateKey) kf.generatePrivate(privateKeySpec);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new JwtException(
                    "cannot generate RSA private/public key par for key with id of " + keyId, e);
        }
    }

    @Override
    public RSAPublicKey getPublicKeyById(String keyId) {
        return publicKey;
    }

    @Override
    public RSAPrivateKey getPrivateKey() {
        return privateKey;
    }

    @Override
    public String getPrivateKeyId() {
        return keyId;
    }
}
