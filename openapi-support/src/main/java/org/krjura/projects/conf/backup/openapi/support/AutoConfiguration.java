package org.krjura.projects.conf.backup.openapi.support;

import org.krjura.projects.conf.backup.openapi.support.controllers.OpenApiController;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;

@Configuration
@ConditionalOnProperty(
        value = "org.krjura.openapi.enabled", havingValue = "true", matchIfMissing = true)
@ConditionalOnClass(ResponseEntity.class)
public class AutoConfiguration {

    @Bean
    public OpenApiController openApiController() {
        return new OpenApiController();
    }
}
