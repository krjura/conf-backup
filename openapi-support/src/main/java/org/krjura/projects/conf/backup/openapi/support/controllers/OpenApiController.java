package org.krjura.projects.conf.backup.openapi.support.controllers;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class OpenApiController {

    @GetMapping(
            value = "${org.krjura.openapi.path:/openapi}",
            produces = MediaType.TEXT_PLAIN_VALUE
    )
    public ResponseEntity<Resource> serve() {
        return ResponseEntity
                .ok()
                .body(new ClassPathResource("openapi.yaml"));
    }
}
