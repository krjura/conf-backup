# conf-backup

## General

This application is a prototype of a file backup service.
It should be used primary for backup of configuration folders on variaty of servers.
At the moment I am using git to store configuration data but it is missing few stuff:

  * data should be encrypted on the client. Server should not contain any information about what that data is and how to encrypt it
  * security should be based per team and folder
  * concept of team should be used to to allow multiple people to the same data
  * folder will be used since data should be encrypted with a separate key (in theory at least)


## Design

application will consist of two components:

  * API service which will allow the client to manage files (CRUD for files)
  * Management service which will allow administration of users
  * Web portal for users

Project will also need a client for CLI access

## Tech stack

vert.x for API service - due to async nature
Spring Boot for Management - due to simplicity
Web as a combination of React.js and server side rendering using Thymeleaf
